class CardapioItsnsModel {
  final int id;
  final String name;
  final String svalue;
  final String mvalue;
  final String lvalue;
  final int small;
  final int medium;
  final int large;
  final int menu_id;


  CardapioItsnsModel(
    this.id,
    this.name,
    this.svalue,
    this.mvalue,
    this.lvalue,
    this.small,
    this.medium,
    this.large,
    this.menu_id
  );
}