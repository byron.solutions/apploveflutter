import 'package:app_love/model/cardapioItensModel.dart';

class CardapioModel {
  final int id;
  final String name;
  final String image;
  final List<CardapioItsnsModel> cardapioItsnsModel;

// named parameters
  CardapioModel(
    this.id,
    this.name,
    this.image,
    this.cardapioItsnsModel,
  );

}