import 'package:app_love/widgets/custom_itemSliver.dart';
import 'package:flutter/material.dart';
import 'package:app_love/model/cardapio.dart';
import 'package:app_love/repositories/cardapio_api.dart';

class HomeTab extends StatefulWidget {
  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  List<CardapioModel> listCardapios = [];
  //List<CardapioModel> resultList = [];
  void loadCardapios()async{
    var ans = await getCardapios();
     setState(() {
       listCardapios.addAll(ans);
    });
    //print(ans);
  }

  @override
  void initState() {
    loadCardapios();
    super.initState();
  }

@override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          expandedHeight: 300.0,
          floating: false,
          pinned: true,
          snap: false,
          backgroundColor: Theme.of(context).primaryColor,
          flexibleSpace: FlexibleSpaceBar(
            background: Image.network("https://images.vexels.com/media/users/3/158443/isolated/preview/c18e72479aefde6c1b5ec0039c866410-silhueta-de-hamb-rguer-logotipo-by-vexels.png",
            fit: BoxFit.fill,),
          ),
          actions: <Widget>[
            IconButton(icon: const Icon(Icons.search),iconSize: 35.0, onPressed: (){
              print("icon search");
            },),
            IconButton(icon: const Icon(Icons.shopping_cart), iconSize: 35.0, onPressed: (){
              print("icon shoppinf=g");
            },)
          ],
        ),
        SliverToBoxAdapter(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start, 
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 15.0,left: 7.0,bottom: 7.0),
                child: Text("CARDÁPIO",
                  style: TextStyle(
                    fontSize: 16.0, 
                    color: Theme.of(context).primaryColor
                  ),
                ),
              ),
              Container(
                height: 2,
                color: Theme.of(context).primaryColor
              )
            ],
          )
        ),
        SliverList( 
        delegate: SliverChildBuilderDelegate(
          (context, index){
          if(listCardapios.isEmpty){
            return Center(
              child: CircularProgressIndicator(),
            );
          }

            if(index < listCardapios.length)
              return buildItem(listCardapios[index],context);
          }
          
        ),
      ),
      
      ],
    );
  }
}