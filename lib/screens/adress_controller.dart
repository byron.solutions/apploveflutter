import 'package:app_love/widgets/modal_accept.dart';
import 'package:flutter/material.dart';

class AddressController {
  final Modal m = Modal();
  static const String edit = "Editar";
  static const String remove = "Remover";

  static const List<String> choices = <String>[edit, remove];

  void choiceAction(String choice, BuildContext context) {
    if (choice == edit) {
      print(("Editar"));
    } else {
      print("Remover");
      showDialog(
          builder: (context) {
            return m.showDialog(context);
          },
          context: context);
    }
  }
}
