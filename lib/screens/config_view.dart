import 'package:flutter/material.dart';

import 'address_view.dart';

class ConfigView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: RaisedButton(
        child: Text("Endereços"),
        onPressed: (){
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context)=> AddressView())
          );
        },
      )
    );
  }
}