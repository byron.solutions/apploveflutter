
import 'package:app_love/screens/config_view.dart';
import 'package:app_love/screens/login_view.dart';
import 'package:app_love/screens/userCreate_view.dart';
import 'package:app_love/tabs/home_tab.dart';
import 'package:app_love/widgets/custom_drawer.dart';
import 'package:flutter/material.dart';

import 'menu_view.dart';

class HomeView extends StatelessWidget {

 
  final _pageController = PageController();
 
  @override
  Widget build(BuildContext context) {
    return SafeArea(
          child: PageView(
        controller: _pageController,
        
        physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Scaffold(
            body: HomeTab(),
            drawer: CustomDrawer(_pageController),
          ),
          Scaffold(
            body: LoginView() ,
            drawer: CustomDrawer(_pageController)
          ),
          Scaffold(
            appBar: AppBar(
              title: Text("Cardapio"),
              centerTitle: true,
            ),
            body: MenuView() ,
            drawer: CustomDrawer(_pageController)
          ),
          Scaffold(
            appBar: AppBar(
              title: Text("Configurações"),
              centerTitle: true,
            ),
            body: ConfigView(),
            drawer: CustomDrawer(_pageController),
          )
        ],
      ),
    );
  }
}