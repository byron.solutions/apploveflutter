import 'package:app_love/model/cardapio.dart';
import 'package:app_love/model/cardapioItensModel.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';

class CardapioItensView extends StatefulWidget {
  @override
  _CardapioItensViewState createState() => _CardapioItensViewState();
}

class _CardapioItensViewState extends State<CardapioItensView> {
  int _radioValue1;
  //itens de cardapio
  //List<CardapioItsnsModel> arguments = [];
  CardapioModel arguments;
  @override
  Widget build(BuildContext context) {
    
    //ESTUDAR
    var settings = ModalRoute.of(context).settings;
    arguments = settings.arguments;
    print(arguments);
    for(var item in arguments.cardapioItsnsModel){
      print(item.name);
    }
    //ESTUDAR
    
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: BackButton(
          color: Theme.of(context).primaryColor,
        ),
        title: Text(
          arguments.name,
          style: TextStyle(color: Theme.of(context).primaryColor),
        ),
        centerTitle: true,
      ),
      bottomNavigationBar: buttomNavigator(),
      body: ListView.separated(
            itemCount: this.arguments.cardapioItsnsModel.length,
            separatorBuilder: (context, index) => Divider(color: Theme.of(context).primaryColor),
            itemBuilder: (context, index) {
              return builditensCardapio(index);
            },
      ),
    );
  }

  //Barra vermelha que fica na parte inferior da tela
  Widget buttomNavigator() {
    return BottomAppBar(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 18.0),
        height: MediaQuery.of(context).size.height * 0.08,
        color: Theme.of(context).primaryColor,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text("0", style: TextStyle(color: Colors.white)),
                  Icon(Icons.card_travel, color: Colors.white),
                ],
              ),
              Text("Ver meu carrinho", style: TextStyle(color: Colors.white)),
              Text("R\$ 00,00", style: TextStyle(color: Colors.white))
            ],
          ),
        ),
      ),
    );
  }

  //Cria os botoes de adicionar e remover uma quantidae
  Widget trailingAmount() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.03,
      width: MediaQuery.of(context).size.width * 0.3,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          InkWell(
            child: Text("-",
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 17.0)),
            onTap: () {},
          ),
          Text("0",
              style: TextStyle(
                  color: Theme.of(context).primaryColor, fontSize: 17.0)),
          InkWell(
            child: Text("+",
                style: TextStyle(
                    color: Theme.of(context).primaryColor, fontSize: 17.0)),
            onTap: () {},
          )
        ],
      ),
    );
  }

  //Itens de cada cardapio
  Widget builditensCardapio(index) {
    return ExpandablePanel(
      header: ListTile(
         leading: Icon(
           Icons.dvr,
           size: 40.0,
         ),
         title: Text(
           "${this.arguments.cardapioItsnsModel[index].name}",
           style: TextStyle(color: Colors.black),
         ),
         subtitle: Text("${this.arguments.cardapioItsnsModel[index].mvalue}"),
         trailing: trailingAmount()),
      expanded: Container(
        child: Column(
          children: <Widget>[
            Text("Descrição e remoção de ingredientes",style: TextStyle(
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).primaryColor
              )
            ),
            Chip(
              label: Text("Pão"),
              onDeleted: (){},
            ),
            Chip(
              label: Text("Carne"),
                onDeleted: (){},
              ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Radio(
                  value: 0,
                  groupValue: _radioValue1,
                  onChanged: (value){},
                ),
                Column(
                  children: <Widget>[
                    Text("Pequeno"),
                    Text("R\$10,00")
                  ],
                ),
                Radio(
                  value: 0,
                  groupValue: _radioValue1,
                  onChanged: (value){},
                ),
                Column(
                  children: <Widget>[
                    Text("Medio"),
                    Text("R\$15,00")
                  ],
                ),
                Radio(
                  value: 0,
                  groupValue: _radioValue1,
                  onChanged: (value){},
                ),
                Column(
                  children: <Widget>[
                    Text("Grande"),
                    Text("R\$20,00")
                  ],
                )
              ],
            )
          ],
        )
      ),
      tapHeaderToExpand: false,
      hasIcon: true,
    );
  }
}

  //Itens de cada cardapio
//   Widget builditensCardapio(index) {
//     return ListTile(
//         leading: Icon(
//           Icons.dvr,
//           size: 40.0,
//         ),
//         title: Text(
//           "${this.arguments.cardapioItsnsModel[index].name}",
//           style: TextStyle(color: Colors.black),
//         ),
//         subtitle: Text("${this.arguments.cardapioItsnsModel[index].mvalue}"),
//         trailing: trailingAmount());
//   }
// }


 // ExpansionTile(
    //   onExpansionChanged: (status){
    //     if(status == true){
    //       print("VERDADE");        
    //     }else{
    //       print("FALSO");
    //     }
    //   },
    //   title: Row(
    //     children: <Widget>[
    //       Stack(
            
    //         children: <Widget>[
    //           Text("click")
    //         ],
    //       )
    //     ],
    //   ),
      
    //   initiallyExpanded: false,
    //   leading: Icon(Icons.add)
    // );