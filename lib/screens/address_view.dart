import 'package:app_love/screens/adress_controller.dart';
import 'package:flutter/material.dart';
class AddressView extends StatelessWidget {
 final AddressController ac = AddressController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Endereços para entrega"),
        centerTitle: true,
      ),
      body: Container(
        child: ListView(
          padding: EdgeInsets.all(10.0),
          children: <Widget>[
            Card(
              color: Colors.grey[350],
              child: ListTile(
                leading: Icon(Icons.location_on,size: 50.0),
                title: Text("Minha casa"),
                subtitle: Text("Rua exemplo, 34\nBairro\nItajuba - MG\nComplemento"),
                trailing: PopupMenuButton<String>(
                  icon: Icon(Icons.more_vert,color: Theme.of(context).primaryColor,),
                  itemBuilder: (BuildContext context){
                    return AddressController.choices.map((String choice){
                      return PopupMenuItem<String>(
                        value: choice,
                        child: ListTile(
                          leading: choice == AddressController.edit ? Icon(Icons.edit,color: Colors.amberAccent[400]) 
                          : Icon(Icons.remove_circle,color: Colors.amberAccent[400]),
                          title: Text(choice),
                        )
                      );
                    }).toList();
                  },
                  onSelected: (String choice){
                    ac.choiceAction(choice,context);
                  },
                ),
                isThreeLine: true,
              ),
            ),
Card(
              color: Colors.grey[350],
              child: ListTile(
                leading: Icon(Icons.location_on,size: 50.0),
                title: Text("Minha casa"),
                subtitle: Text("Rua exemplo, 34\nBairro\nItajuba - MG\nComplemento"),
                trailing: PopupMenuButton<String>(
                  icon: Icon(Icons.more_vert,color: Theme.of(context).primaryColor,),
                  itemBuilder: (BuildContext context){
                    return AddressController.choices.map((String choice){
                      return PopupMenuItem<String>(
                        value: choice,
                        child: ListTile(
                          leading: choice == AddressController.edit ? Icon(Icons.edit) : Icon(Icons.remove_circle),
                          title: Text(choice),
                        )
                      );
                    }).toList();
                  },
                  onSelected: (String choice){
                    ac.choiceAction(choice,context);
                  },
                ),
                isThreeLine: true,
              ),
            ),
Card(
              color: Colors.grey[350],
              child: ListTile(
                leading: Icon(Icons.location_on,size: 50.0),
                title: Text("Minha casa"),
                subtitle: Text("Rua exemplo, 34\nBairro\nItajuba - MG\nComplemento"),
                trailing: PopupMenuButton<String>(
                  icon: Icon(Icons.more_vert,color: Theme.of(context).primaryColor,),
                  itemBuilder: (BuildContext context){
                    return AddressController.choices.map((String choice){
                      return PopupMenuItem<String>(
                        value: choice,
                        child: ListTile(
                          leading: choice == AddressController.edit ? Icon(Icons.edit) : Icon(Icons.remove_circle),
                          title: Text(choice),
                        )
                      );
                    }).toList();
                  },
                  onSelected: (String choice){
                    ac.choiceAction(choice,context);
                  },
                ),
                isThreeLine: true,
              ),
            ),

            Divider( color: Colors.black),
            InkWell(
              child:Align(
                child:  Text("Adicionar endereço",
                style: TextStyle(color: Theme.of(context).primaryColor,
                  fontWeight: FontWeight.w500
                )),
              ),
                              onTap: (){
                print("Click");
              },
            ),
            Divider( color: Colors.black),
          ],
        ),
      )
    );
  }
}

  