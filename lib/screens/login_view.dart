import 'package:app_love/screens/home_view.dart';
import 'package:app_love/screens/userCreate_view.dart';
import 'package:app_love/widgets/custom_formField.dart';
import 'package:flutter/material.dart';

class LoginView extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();

  CustomDecoration decoration = CustomDecoration();

  @override
  Widget build(BuildContext context) {
    return Material(
        child: Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(
                  "http://htc-wallpaper.com/wp-content/uploads/2013/11/Hamburger22.jpg"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Scaffold(
            backgroundColor: Colors.transparent,
            body: SingleChildScrollView(
                child: Align(
                    child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                                SizedBox(
                  height: 30.0,
                ),
                Icon(Icons.fastfood,size: 100.0,),
                SizedBox(height: 30.0,),
                Container(
                  padding: EdgeInsets.all(15.0),
                  width: 315.0,
                  height: 405.0,
                  decoration: BoxDecoration(
                      color: Color.fromRGBO(255, 255, 255, 0.63),
                      border: Border.all(
                        width: 3.0,
                        color: Color.fromRGBO(255, 255, 255, 0.0),
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(40.0))),
                  child: Column(
                    children: <Widget>[
                      Text(
                        "LOGIN",
                        style: TextStyle(
                            decoration: TextDecoration.none,
                            fontSize: 20.0,
                            color: Theme.of(context).primaryColor),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Form(
                        key: _formKey,
                        child: Column(
                          children: <Widget>[
                            TextFormField(
                              validator: (value){
                                if(value.isEmpty) return "email invalido";
                              },
                              decoration: decoration.toInputDecoration(context, Color.fromARGB(255, 118, 4, 12), "Nome", true),
                            ),
                            Divider(),
                            TextFormField(
                              validator: (value){
                                if(value.isEmpty) return "senha invalida";
                              },                              
                              obscureText: true,
                              decoration: decoration.toInputDecoration(context, Color.fromARGB(255, 118, 4, 12), "Senha", true),
                            ),
                            Divider(),
                          ],
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Checkbox(
                            value: false,
                            onChanged: (bool value){},
                          ),
                          Expanded(
                            child:Text(
                            "Lembrar",
                            style: TextStyle(fontSize: 13.0,color: Theme.of(context).primaryColor),
                          ),
                          ),
                          Text(
                            "Esqueci minha senha",
                            style: TextStyle(fontSize: 13.0),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          IconButton(
                            onPressed: (){
                              Navigator.of(context).push(
                                MaterialPageRoute( builder:
                                (context)=>HomeView()
                              ));
                            },
                            icon: Icon(Icons.keyboard_return),
                          ),
                          Expanded(
                            child: RaisedButton(
                              child: Text(
                                "Entrar",
                                style: TextStyle(color: Colors.white),
                              ),
                              color: Theme.of(context).primaryColor,
                              onPressed: () {
                                _formKey.currentState.validate();
                              },
                            ),
                          ),
                        ],
                      ),
                      FlatButton(
                        child: Text("Registrar"),
                        onPressed: (){
                          Navigator.of(context).push(
                              MaterialPageRoute(
                              builder: (context) => UserCreateView()
                            )
                          );
                        },
                      )
                    ],
                  ),
                ),
              ],
            ))))
      ],
    ));
  }
}
