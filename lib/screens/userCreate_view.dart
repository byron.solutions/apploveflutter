import 'package:app_love/widgets/custom_formField.dart';
import 'package:flutter/material.dart';
class UserCreateView extends StatefulWidget {


  @override
  _UserCreateViewState createState() => _UserCreateViewState();
}

class _UserCreateViewState extends State<UserCreateView> {
   final TextEditingController nameController = TextEditingController();
   final _formKey = GlobalKey<FormState>();
  CustomDecoration customDecoration = CustomDecoration();

  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title:  Text("Registrar")) ,body:
    SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15.0),
          child: Column(
            children: <Widget>[
            Icon(Icons.insert_comment,size: 100.0,color: Theme.of(context).primaryColor,),
          //Image.asset("assets/images/diagram.png",width: 200,height: 200),
          Text("CADASTRO POR EMAIL",
                style: TextStyle(
                  letterSpacing: 2.0
                )),
              Divider(color: Theme.of(context).primaryColor,),
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      controller: nameController,
                      decoration: customDecoration.toInputDecoration(context, Color.fromARGB(255, 118, 4, 12), "Nome", false),
                      validator: (value){
                        if(value.isEmpty) return "nome";
                      },
                    ),
                    TextFormField(
                      decoration: customDecoration.toInputDecoration(context, Color.fromARGB(255, 118, 4, 12), "Telefone", false),
                      validator: (value){
                        if(value.isEmpty) return "telefone";
                      },
                    ),
                    TextFormField(
                      decoration: customDecoration.toInputDecoration(context, Color.fromARGB(255, 118, 4, 12), "E-mail", false),
                      validator: (value){
                       if(value.isEmpty) return "email";
                      },
                    ),
                    TextFormField(
                      decoration: customDecoration.toInputDecoration(context, Color.fromARGB(255, 118, 4, 12), "Endereço", false),
                      validator: (value){
                        if(value.isEmpty) return "endereço";
                      },
                    ),
                    TextFormField(
                      obscureText: true,
                      validator: (value){
                        if(value.isEmpty) return "senha";
                      },
                      decoration: customDecoration.toInputDecoration(context, Color.fromARGB(255, 118, 4, 12), "Senha", false),
                    )
                  ],
                ),
               ),
               FlatButton(
                 child: Text("Ok",style: TextStyle(fontSize: 18.0,color: Theme.of(context).primaryColor, fontWeight: FontWeight.w500)),
                 shape: StadiumBorder(),
                 onPressed: (){
                   _formKey.currentState.validate();
                 },
               )
            ],
          )
        ),
      ));
  }
}