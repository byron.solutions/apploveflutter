import 'package:flutter/material.dart';
class CustomDecoration{
    InputDecoration toInputDecoration(context, Color color, String label, bool border){
    return InputDecoration(
            labelText: label,
            labelStyle: TextStyle(
                color: color),
            border: border ? OutlineInputBorder() : UnderlineInputBorder(),
            enabledBorder: border ? OutlineInputBorder(borderSide: BorderSide(color: color)) :
                                    UnderlineInputBorder(borderSide: BorderSide(color: color))
            );
  }  
}