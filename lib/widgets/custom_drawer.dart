import 'package:app_love/tiles/drawer_tile.dart';
import 'package:flutter/material.dart';

class CustomDrawer extends StatelessWidget {

  final PageController pageController;

  CustomDrawer(this.pageController);
  @override
  Widget build(BuildContext context) {

    //Create gradient back drawer
    Widget _buildBrawerBack(){
      return Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              Color.fromARGB(255, 118, 4, 12),
              Color.fromARGB(170, 148, 16, 23),
            ],
            begin: Alignment.topRight,
            end: Alignment.bottomLeft
          )
        ),
      );
      
    }

    return Drawer(
      child: Stack(
        children: <Widget>[
          _buildBrawerBack(),
          ListView(
            padding: EdgeInsets.only(top: 80.0,left: 20.0),
            children: <Widget>[
              DrawerTile(Icons.people,"Entrar",pageController,1),
              Divider(
                color: Colors.white,
                height: 10.0,
                endIndent: 20.0,
              ),
              DrawerTile(Icons.home, "Inicio",pageController,0),
              DrawerTile(Icons.restaurant_menu, "Cardáṕio",pageController,2),
              DrawerTile(Icons.devices_other,"Pedidos",pageController,7),
              DrawerTile(Icons.settings, "Configurações",pageController,3)

            ],
            
          ),
          Positioned(
            top: 30.0,
            left: 10.0,
            child: IconButton(
              icon: Icon(Icons.close),
              iconSize: 30,
              highlightColor: Colors.black,
              color: Colors.white,
              onPressed: (){
                Navigator.of(context).pop();
              },
            )
          ),
          Positioned(
            bottom: 20,
            left: 20,
            child: DrawerTile(Icons.place,"Endereço do estabelecimento",pageController,5),
          )
        ],
      ),
    );
  }
}