import 'package:app_love/model/cardapio.dart';
import 'package:flutter/material.dart';
  Widget buildItem(CardapioModel c, BuildContext context){
      return InkWell(
              onTap: (){
                Navigator.pushNamed(context, "/cardapioItens",arguments: c);
                //c.cardapioItsnsModel
              },
              child: Container(
                height: 150.0,
                child: Padding(
                  padding: EdgeInsets.only(top: 10.0),
                  child: Stack(
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: c.image != null ? NetworkImage(c.image) : NetworkImage("http://134.209.173.98/storage/uploads/vCwsPlSW24ihAZWmXr5M4URh8aeEm5Udq8hmxPq4.jpeg"),
                          fit: BoxFit.cover
                        )
                      ),
                    ),
              
                    Container(
                      color: Color.fromRGBO(0, 0, 0, 0.5),
                    ),

                    Align(
                      child: Text(c.name,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w500
                        ),
                      ),
                    )
                  ],
                ),
                ),
        ),
      );
  }