import 'package:flutter/material.dart';

class Modal{
     showDialog(BuildContext context){
        return AlertDialog(
          title: new Text("Remover Endereço"),
          content: new Text("Ao confirmar o endereço será removido. Clique em cancelar para manter o endereço."),
          actions: <Widget>[
          new FlatButton(
              child: new Text("Remover", style: TextStyle(color: Colors.black45),),
              onPressed: () {
                
              },
            ),
            new FlatButton(
              child: new Text("Cancelar",style: TextStyle(color: Theme.of(context).primaryColor),),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
  }
}