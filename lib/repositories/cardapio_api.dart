import 'package:app_love/model/cardapioItensModel.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:app_love/model/cardapio.dart';


Future<List<CardapioModel>> getCardapios() async {

  String url = "http://165.227.20.15/api/v1/cardapio";
   List<CardapioModel> listCadapios = [];
   
  try{

  var data = await http.get(url);
  var jsonData = json.decode(data.body)["data"];

  for (var c in jsonData) {
    List<CardapioItsnsModel> listCadapiosItens = [];

    for(var p in c["products_in_menu"]){
      CardapioItsnsModel cim = CardapioItsnsModel(p["id"], p["name"], p["svalue"], p["mvalue"], p["lvalue"], p["small"], p["medium"], p["large"], p["menu_id"]);
      listCadapiosItens.add(cim);

    }

    CardapioModel cardapio = CardapioModel(c["id"],c["name"],c["image"],listCadapiosItens);
    listCadapios.add(cardapio);
  }

  }catch(erro){
    print(erro);
  }
  
  return listCadapios;
}