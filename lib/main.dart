
import 'package:app_love/screens/cardapioItens_view.dart';
import 'package:app_love/screens/home_view.dart';
import 'package:app_love/screens/login_view.dart';
import 'package:app_love/screens/userCreate_view.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Love',
      theme: ThemeData(
        //primarySwatch: Colors.blue,
        hintColor: Color.fromARGB(255, 118, 4, 12),
        primaryColor: Color.fromARGB(255, 118, 4, 12)
      ),
      debugShowCheckedModeBanner: false,
      home: HomeView(),
      routes: <String, WidgetBuilder>{
        "/home" : (context)=> HomeView(),
        "/login": (context)=> LoginView(),
        "/cardapioItens": (context)=> CardapioItensView(),
      },
    );
  }
}